<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="./js/jQuery.min.js"></script>
<title>簡易Twitter</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>
		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="account">
					@
					<c:out value="${loginUser.account}" />
				</div>
				<div class="description">
					<c:out value="${loginUser.description}" />
				</div>
			</div>
		</c:if>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="date">
			<span>日付:</span>
			<form action="./" method="get">
				<input name="start-date" id="start-date" type="date" value="${ startDate }" />
					<input name="end-date" id="end-date" type="date" value="${ endDate }" />
					 <input name="narrow"value="絞り込み" id="date" type="submit" />
			</form>
		</div>
		<br>

		<div class="form-area">
			<c:if test="${ isShowMessageForm }">
				<form action="message" method="post">
					いま、どうしてる？<br />
					<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
					<br /> <input type="submit" value="つぶやく">（140文字まで）
				</form>
			</c:if>
		</div>
		<br>

		<!-- userMessageから取得したものを表示 -->
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="account-name">
						<span class="account"><a
							href="./?user_id=<c:out value="${message.userId}"/> "><c:out
									value="${message.account}" /></a></span>
						<!-- 変数をmessageのuserIdメソッドを用いてセット,選択したユーザーだけを表示するurlを作成、user_idのパラメーターを渡している-->
						<span class="name"><c:out value="${message.name}" /></span>
					</div>
					<div class="text">
						<pre><c:out value="${message.text}" /></pre> <!-- pre　タグ内のテキストを整形して表示 -->
					</div>
					<div class="date">
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
					<div class="deleteMessage">
						<c:if test="${message.userId == loginUser.id }">
							<form action="deleteMessage" method="post">
								<input name="id" value="${message.id}" id="id" type="hidden" />
								<input type="submit" name="deleteMessage" value="削除">
							</form>
						</c:if>
					</div>

					<div class="edit">
						<c:if test="${message.userId == loginUser.id }">
						<form action="edit">
								<input name="id" value="${message.id}" id="id" type="hidden" />
								<a href="edit"><input type="submit" value="編集"></a>

								</form>
						</c:if>
					</div>

					<div class="comment-area">
						<c:if test="${ isShowMessageForm }">
							<form action="comment" method="post">
								<input name="id" value="${message.id}" id="id" type="hidden" />
								<textarea name="text" cols="100" rows="5" class="comment-box"></textarea>
								<br /> <input type="submit" value="返信">（140文字まで）
							</form>
						</c:if>
						<c:forEach items="${comments}" var="comment">
							<c:if test="${comment.messageId == message.id }">
								<br>
								<span class="account"><c:out value="${comment.account}" /></span>
								<span class="name"><c:out value="${comment.name}" /></span>
								<br>
								<pre><span class="text"><c:out value="${comment.text}" /></span></pre>
								<br>
								<div class="date">
									<fmt:formatDate value="${comment.createdDate}"
										pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
							</c:if>
						</c:forEach>
					</div>
				</div>
				<br>
			</c:forEach>
		</div>
		<div class="copyright">Copyright(c)RyoTsujimura</div>
	</div>
</body>
</html>