package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.beans.UserMessage;
import chapter6.service.MessageService;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//セッションを開始し、userにログイン情報を格納

		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<String>();

		String id = request.getParameter("id");

		if (!isGetValid(id, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		UserMessage message = getMessage(request);
		message = new MessageService().select(message.getId());
		message.getText();

		User user = new UserService().select(loginUser.getId());

		request.setAttribute("user", user);
		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		User loginUser = (User) session.getAttribute("loginUser");

		UserMessage message = getMessage(request);

		if (!isPostValid(message.getText(), errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);

			return;
		}

		new MessageService().update(message);

		session.setAttribute("loginUser", loginUser);

		response.sendRedirect("./");
	}

	private boolean isGetValid(String id, List<String> errorMessages) {

		if (StringUtils.isBlank(id)) {
			errorMessages.add("不正なパラメーターが入力されました");
			return false;
		} else if (!(id.matches("^[0-9]*$"))) {
			errorMessages.add("不正なパラメーターが入力されました");
			return false;
		}
		UserMessage editMessage = new MessageService().editMessages(Integer.parseInt(id));
		if ((editMessage == null)) {
			errorMessages.add("不正なパラメーターが入力されました");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

	private boolean isPostValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

	private UserMessage getMessage(HttpServletRequest request) throws IOException, ServletException {

		UserMessage message = new UserMessage();
		message.setId(Integer.parseInt(request.getParameter("id")));
		message.setText(request.getParameter("text"));
		return message;
	}

}
