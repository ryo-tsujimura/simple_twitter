package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/deleteMessage" }) //このjspファイルにアクセスしたときに実行される
public class DeleteMessageServlet extends HttpServlet {

	@Override //スーパークラスのメソッドを上書きしている
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String id = request.getParameter("id");
		Message deleteMessage = new Message();

		deleteMessage.setId(Integer.parseInt(id));

		new MessageService().delete(deleteMessage);
		response.sendRedirect("./");
	}
}