package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" }) //ウェルカムページ(Topページ)を開いたときに実行される
public class TopServlet extends HttpServlet {

	@Override //スーパークラスのメソッドを上書きしている
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		boolean isShowMessageForm = false;//セッションでオブジェクトが取得できる(ログイン状態)にあれば投稿画面を表示
		User user = (User) request.getSession().getAttribute("loginUser");

		if (user != null) {
			isShowMessageForm = true;
		}

		String startDate = request.getParameter("start-date");
		String endDate = request.getParameter("end-date");

		String userId = request.getParameter("user_id");

		List<UserMessage> messages = new MessageService().select(userId, startDate, endDate);//表結合したデータをここでリスト化しjspで表示

		List<UserComment> comments = new CommentService().select();//表結合したデータをここでリスト化しjspで表示

		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("isShowMessageForm", isShowMessageForm);

		request.getRequestDispatcher("/top.jsp").forward(request, response);//取得したリクエストをjspにフォワード
	}
}