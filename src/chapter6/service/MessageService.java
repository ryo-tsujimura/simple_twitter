package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId, String startDate, String endDate) {
		final int LIMIT_NUM = 1000;

		Timestamp startTime = new Timestamp(2020 - 01 - 01 - 00 - 00 - 00 - 00);//Timestmp型のインスタンス作成のための型
		Timestamp endTime = new Timestamp(System.currentTimeMillis());

		if (StringUtils.isNotEmpty(startDate)) {
			startTime = Timestamp.valueOf(startDate + " 00:00:00");//DBのタイムスタンプ型用の型
		}

		if (StringUtils.isNotEmpty(endDate)) {
			endTime = Timestamp.valueOf(endDate + " 23:59:59");
		}

		Connection connection = null;
		try {
			connection = getConnection();

			Integer id = null;
			if (!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startTime,
					endTime);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(Message deleteMessage) {

		Connection connection = null;
		try {
			connection = getConnection();

			new MessageDao().delete(connection, deleteMessage);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(UserMessage editMessage) {

		Connection connection = null;
		try {
			connection = getConnection();

			new UserMessageDao().update(connection, editMessage);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public UserMessage select(int id) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserMessage message = new UserMessageDao().select(connection, id);
			commit(connection);

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public UserMessage editMessages(int id) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserMessage message = new UserMessageDao().selectEditUrl(connection, id);
			commit(connection);

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}