package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserMessage;
import chapter6.exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> select(Connection connection, Integer id, int num, Timestamp startTime,
			Timestamp endTime) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT ");
			sql.append("    messages.id as id, ");
			sql.append("    messages.text as text, ");
			sql.append("    messages.user_id as user_id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name, ");
			sql.append("    messages.created_date as created_date ");
			sql.append(" FROM messages ");
			sql.append(" INNER JOIN users ");
			sql.append(" ON messages.user_id = users.id ");
			sql.append(" WHERE messages.created_date BETWEEN ? AND ? ");

			if (id != null) {
				sql.append("AND user_id = ?");
			}
			sql.append(" ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ps.setTimestamp(1, startTime);
			ps.setTimestamp(2, endTime);

			if (id != null) {
				ps.setInt(3, id);
			}

			ResultSet rs = ps.executeQuery();

			List<UserMessage> messages = toUserMessages(rs);

			return messages;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

		List<UserMessage> messages = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				UserMessage message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setText(rs.getString("text"));
				message.setUserId(rs.getInt("user_id"));
				message.setAccount(rs.getString("account"));
				message.setName(rs.getString("name"));
				message.setCreatedDate(rs.getTimestamp("created_date"));

				messages.add(message);
			}
			return messages;
		} finally {
			close(rs);
		}
	}

	public UserMessage select(Connection connection, int id) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT ");
			sql.append("    messages.id as id, ");
			sql.append("    messages.text as text ");
			sql.append(" FROM messages ");
			sql.append(" WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			List<UserMessage> editMessages = toUserEditMessages(rs);

			return editMessages.get(0);

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public UserMessage selectEditUrl(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = " SELECT * FROM messages WHERE id = ? ";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			List<UserMessage> editMessages = toUserEditMessages(rs);
			if (editMessages.isEmpty()) {
				return null;
			} else if (2 <= editMessages.size()) {
				throw new IllegalStateException("メッセージIDが重複しています");
			} else {
				return editMessages.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserEditMessages(ResultSet rs) throws SQLException {

		List<UserMessage> messages = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				UserMessage message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setText(rs.getString("text"));

				messages.add(message);
			}
			return messages;
		} finally {
			close(rs);
		}
	}

	public void update(Connection connection, UserMessage editMessage) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" UPDATE  messages  SET ");
			sql.append("	text =  ?, ");
			sql.append("    updated_date = CURRENT_TIMESTAMP "); // updated_date
			sql.append(" WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, editMessage.getText());
			ps.setInt(2, editMessage.getId());


			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}